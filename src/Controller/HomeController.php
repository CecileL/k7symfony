<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movies;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="vitrine")
     */
    public function vitrine() {
        

        if (!isset($_SESSION)){
        session_start();
        return $this->render('vitrine/vitrine.html.twig', [
            'session' => session_id(),
            ]);
        } else {
            return $this->render('vitrine/vitrine.html.twig', [
                'session' => session_id(),
                ]);
        }
    }

    /**
     * @Route("/home", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }
    
    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription()
    {
        return $this->render('inscription/inscription.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion()
    {
        return $this->render('connexion/connexion.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

        /**
     * @Route("/log_out", name="deconnexion")
     */
    public function deconnexion()
    {
        if (isset($_SESSION)){
            session_destroy();
            return $this->render('vitrine/vitrine.html.twig', [
                'test' => 'oui',
                ]);
            } else {
                return $this->render('vitrine/vitrine.html.twig', [
                    'test' => 'echec',
                    ]);
            }
    }
}
