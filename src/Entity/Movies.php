<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MoviesRepository")
 */
class Movies
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $resume;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $released;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $runtime;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $director;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $awards;

    /**
     * @ORM\Column(type="text")
     */
    private $poster;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dvd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $production;

    /**
     * @ORM\Column(type="boolean")
     */
    private $avalaible;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MovieOrder", mappedBy="movie_id")
     */
    private $movieOrders;

    public function __construct()
    {
        $this->movieOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getReleased(): ?string
    {
        return $this->released;
    }

    public function setReleased(string $released): self
    {
        $this->released = $released;

        return $this;
    }

    public function getRuntime(): ?string
    {
        return $this->runtime;
    }

    public function setRuntime(string $runtime): self
    {
        $this->runtime = $runtime;

        return $this;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getDirector(): ?string
    {
        return $this->director;
    }

    public function setDirector(string $director): self
    {
        $this->director = $director;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getAwards(): ?string
    {
        return $this->awards;
    }

    public function setAwards(?string $awards): self
    {
        $this->awards = $awards;

        return $this;
    }

    public function getPoster(): ?string
    {
        return $this->poster;
    }

    public function setPoster(string $poster): self
    {
        $this->poster = $poster;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDvd(): ?string
    {
        return $this->dvd;
    }

    public function setDvd(string $dvd): self
    {
        $this->dvd = $dvd;

        return $this;
    }

    public function getProduction(): ?string
    {
        return $this->production;
    }

    public function setProduction(string $production): self
    {
        $this->production = $production;

        return $this;
    }

    public function getAvalaible(): ?bool
    {
        return $this->avalaible;
    }

    public function setAvalaible(bool $avalaible): self
    {
        $this->avalaible = $avalaible;

        return $this;
    }

    /**
     * @return Collection|MovieOrder[]
     */
    public function getMovieOrders(): Collection
    {
        return $this->movieOrders;
    }

    public function addMovieOrder(MovieOrder $movieOrder): self
    {
        if (!$this->movieOrders->contains($movieOrder)) {
            $this->movieOrders[] = $movieOrder;
            $movieOrder->setMovieId($this);
        }

        return $this;
    }

    public function removeMovieOrder(MovieOrder $movieOrder): self
    {
        if ($this->movieOrders->contains($movieOrder)) {
            $this->movieOrders->removeElement($movieOrder);
            // set the owning side to null (unless already changed)
            if ($movieOrder->getMovieId() === $this) {
                $movieOrder->setMovieId(null);
            }
        }

        return $this;
    }
}
