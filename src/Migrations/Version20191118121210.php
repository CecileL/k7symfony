<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191118121210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE roles (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movies (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, resume LONGTEXT NOT NULL, year VARCHAR(255) NOT NULL, released VARCHAR(255) NOT NULL, runtime VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, director VARCHAR(255) NOT NULL, language VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, awards VARCHAR(255) DEFAULT NULL, poster LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, dvd VARCHAR(255) NOT NULL, production VARCHAR(255) NOT NULL, avalaible TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_order (id INT AUTO_INCREMENT NOT NULL, movie_id_id INT NOT NULL, order_id_id INT NOT NULL, INDEX IDX_8B6B890410684CB (movie_id_id), INDEX IDX_8B6B8904FCDAEAAA (order_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_user (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, role_id_id INT NOT NULL, INDEX IDX_332CA4DD9D86650F (user_id_id), INDEX IDX_332CA4DD88987678 (role_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE orders (id INT AUTO_INCREMENT NOT NULL, user_id_id INT NOT NULL, reference VARCHAR(255) NOT NULL, INDEX IDX_E52FFDEE9D86650F (user_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_order ADD CONSTRAINT FK_8B6B890410684CB FOREIGN KEY (movie_id_id) REFERENCES movies (id)');
        $this->addSql('ALTER TABLE movie_order ADD CONSTRAINT FK_8B6B8904FCDAEAAA FOREIGN KEY (order_id_id) REFERENCES orders (id)');
        $this->addSql('ALTER TABLE role_user ADD CONSTRAINT FK_332CA4DD9D86650F FOREIGN KEY (user_id_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE role_user ADD CONSTRAINT FK_332CA4DD88987678 FOREIGN KEY (role_id_id) REFERENCES roles (id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE9D86650F FOREIGN KEY (user_id_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE role_user DROP FOREIGN KEY FK_332CA4DD88987678');
        $this->addSql('ALTER TABLE movie_order DROP FOREIGN KEY FK_8B6B890410684CB');
        $this->addSql('ALTER TABLE role_user DROP FOREIGN KEY FK_332CA4DD9D86650F');
        $this->addSql('ALTER TABLE orders DROP FOREIGN KEY FK_E52FFDEE9D86650F');
        $this->addSql('ALTER TABLE movie_order DROP FOREIGN KEY FK_8B6B8904FCDAEAAA');
        $this->addSql('DROP TABLE roles');
        $this->addSql('DROP TABLE movies');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE movie_order');
        $this->addSql('DROP TABLE role_user');
        $this->addSql('DROP TABLE orders');
    }
}
