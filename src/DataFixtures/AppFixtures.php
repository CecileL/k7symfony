<?php

namespace App\DataFixtures;

use App\Entity\Movies;
use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // user
        $user = new Users();
        $user->setName('admin');
        $user->setEmail('admin@example.fr');
        $user->setPassword('admin');
        // user
        // movie
        $movie = new Movies();
        $movie->setTitle('Drive');
        $movie->setResume("Un jeune homme solitaire, The Driver, conduit le jour à Hollywood pour le cinéma en tant que cascadeur et la nuit pour des truands. Ultra professionnel et peu bavard, il a son propre code de conduite. Jamais il n'a pris part aux crimes de ses employeurs autrement qu'en conduisant - et au volant, il est le meilleur !.");
        $movie->setYear('2011');
        $movie->setReleaseD('09-16-2011');
        $movie->setRuntime('100');
        $movie->setGenre('Crime, Drama');
        $movie->setDirector('Nicolas Winding Refn');
        $movie->setLanguage('Anglish, Spanish');
        $movie->setCountry('USA');
        $movie->setAwards('Nominated for 1 Oscar. Another 77 wins & 170 nominations.');
        $movie->setPoster('https://m.media-amazon.com/images/M/MV5BZjY5ZjQyMjMtMmEwOC00Nzc2LTllYTItMmU2MzJjNTg1NjY0XkEyXkFqcGdeQXVyNjQ1MTMzMDQ@._V1_SX300.jpg');
        $movie->setType('Movie');
        $movie->setDvd('01-30-2012');
        $movie->setProduction('Bold Films');
        $movie->setAvalaible(true);

        $movie1 = new Movies();
        $movie1->setTitle('hancock');
        $movie1->setResume("Il y a les héros, les super-héros et il y a Hancock. Ses super-pouvoirs lui ont permis de sauver d'innombrables vies, mais les dégâts monstrueux qu'il fait au passage ont fini par le rendre impopulaire. Les habitants de Los Angeles n'en peuvent plus.");
        $movie1->setYear('2008');
        $movie1->setReleaseD('02 Jul 2008');
        $movie1->setRuntime('92');
        $movie1->setGenre('Action, Fantasy');
        $movie1->setDirector('Peter Berg');
        $movie1->setLanguage('Vietnamese, English, Japaneseh');
        $movie1->setCountry('USA');
        $movie1->setAwards('4 wins & 11 nominations.');
        $movie1->setPoster('https://m.media-amazon.com/images/M/MV5BMTgyMzc4ODU3NV5BMl5BanBnXkFtZTcwNjk5Mzc1MQ@@._V1_SX300.jpg');
        $movie1->setType('Movie');
        $movie1->setDvd('25 Nov 2008');
        $movie1->setProduction('Columbia Pictures');
        $movie1->setAvalaible(true);

        $movie2 = new Movies();
        $movie2->setTitle('Bad Boy');
        $movie2->setResume("Doug Dawg Menford, un homme à femmes, doit s'excuser auprès de toutes les conquêtes féminines qu'il a trahi et ce, pour hériter d'un million de dollars légué par sa grand-mère avant son décès. Anna Lockhart, une avocate, le suivra au cours de sa quête pour vérifier si ce dernier respecte bien les volontés de la défunte.");
        $movie2->setYear('2002');
        $movie2->setReleaseD('21 Feb 2002');
        $movie2->setRuntime('83');
        $movie2->setGenre('Drama, Comedy, Romance');
        $movie2->setDirector('Victoria Hochberg');
        $movie2->setLanguage('English');
        $movie2->setCountry('USA');
        $movie2->setAwards('1 nomination');
        $movie2->setPoster('https://m.media-amazon.com/images/M/MV5BMTg2MzU1NjIzNV5BMl5BanBnXkFtZTcwMDYwMTQyMQ@@._V1_SX300.jpg');
        $movie2->setType('Movie');
        $movie2->setDvd('30 Sep 2003');
        $movie2->setProduction('"Gold Circle Films');
        $movie2->setAvalaible(true);

        $movie3 = new Movies();
        $movie3->setTitle('Beethoven');
        $movie3->setResume("La famille Newton a tout pour être heureuse. Pourtant, elle sent qu'il lui manque quelque chose. Ce manque va être comblé par un chiot, tout juste échappé des griffes de ravisseurs de chiens. Les Newton baptisent le chiot Beethoven et celui-ci grossit, grossit, au point d'atteindre 85 kilos ! Pendant ce temps, le docteur Varnick, un terrible vétérinaire, met au point en secret des produits qui nécessitent des expériences sur les chiens.");
        $movie3->setYear('1992');
        $movie3->setReleaseD('03 Apr 1992');
        $movie3->setRuntime('87');
        $movie3->setGenre('Comedy, Drama, Family');
        $movie3->setDirector('Brian Levant');
        $movie3->setLanguage('English');
        $movie3->setCountry('USA');
        $movie3->setAwards('1 win & 4 nominations.');
        $movie3->setPoster('https://m.media-amazon.com/images/M/MV5BODgwY2MyMTItMzY1OC00YmU3LTlhOWMtYWFkNGJjNzIwOWZiXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg');
        $movie3->setType('Movie');
        $movie3->setDvd('04 Jul 2000');
        $movie3->setProduction('Universal Pictures');
        $movie3->setAvalaible(true);

        $movie4 = new Movies();
        $movie4->setTitle('Titanic');
        $movie4->setResume("En 1997, l'épave du Titanic est l'objet d'une exploration fiévreuse, menée par des chercheurs de trésor en quête d'un diamant bleu qui se trouvait à bord. Frappée par un reportage télévisé, l'une des rescapés du naufrage, âgée de 102 ans, Rose DeWitt, se rend sur place et évoque ses souvenirs. 1912. Fiancée à un industriel arrogant, Rose croise sur le bateau un artiste sans le sou.");
        $movie4->setYear('1997');
        $movie4->setReleaseD('19 Dec 1997');
        $movie4->setRuntime('194');
        $movie4->setGenre('Drama, Romance');
        $movie4->setDirector('James Cameron');
        $movie4->setLanguage('English, Swedish, Italian');
        $movie4->setCountry('USA');
        $movie4->setAwards('Won 11 Oscars. Another 111 wins & 77 nominations.');
        $movie4->setPoster('https://m.media-amazon.com/images/M/MV5BMDdmZGU3NDQtY2E5My00ZTliLWIzOTUtMTY4ZGI1YjdiNjk3XkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_SX300.jpg');
        $movie4->setType('Movie');
        $movie4->setDvd('10 Sep 2012');
        $movie4->setProduction('Paramount Pictures');
        $movie4->setAvalaible(true);
        // movie




        // persistance -> ajoutez la variables entre parenthèse
        $manager->persist($user);
        $manager->persist($movie);
        $manager->persist($movie1);
        $manager->persist($movie2);
        $manager->persist($movie3);
        $manager->persist($movie4);


        $manager->flush();
    }
}
